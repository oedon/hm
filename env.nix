{
  home.sessionVariables = {
    EDITOR = "nvim";
    VISUAL = "nvim";
    MANPAGER = "nvim +Man!";
    MANWIDTH = "999";
    LESSHISTFILE = "-";

    XDG_CACHE_HOME = "\${HOME}/.cache";
    XDG_CONFIG_HOME = "\${HOME}/.config";
    XDG_BIN_HOME = "\${HOME}/.local/bin";
    XDG_DATA_HOME = "\${HOME}/.local/share";

    HISTFILE = "\${HOME}/.cache/zsh/history";
    CUDA_CACHE_PATH = "\${HOME}/.cache/nv";
    PASSWORD_STORE_DIR = "\${HOME}/.local/share/password-store";
    GOPATH = "\${HOME}/go";
    GOBIN = "\${HOME}/go/bin";
    _JAVA_AWT_WM_NONREPARENTING = "1";

    CARGO_HOME = "\${HOME}/.cargo";
    RUSTUP_HOME = "\${HOME}/.rustup";

  };

}
