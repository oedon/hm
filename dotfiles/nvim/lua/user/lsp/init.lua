local lsp_status_ok, lsp = pcall(require, "lsp-zero")
if not lsp_status_ok then
    return
end

lsp.preset('recommended')

lsp.on_attach(function(client, bufnr)
    local noremap = { buffer = bufnr, remap = false }
    local bind = vim.keymap.set

    bind("n", "<leader>r", "<cmd>lua vim.lsp.buf.rename()<CR>", noremap)
    bind("n", "gl", "<cmd>lua vim.diagnostic.open_float()<CR>", noremap)
    -- more code  ...
end)

-- Set default keymaps
lsp.set_preferences({
    set_lsp_keymaps = true
})

-- CMP Setup
lsp.setup_nvim_cmp({
    formatting = {
        -- changing the order of fields so the icon is the first
        fields = { 'menu', 'abbr', 'kind' },
        -- here is where the change happens
        format = function(entry, item)
            local menu_icon = {
                nvim_lsp = 'λ',
                luasnip = '⋗',
                buffer = 'Ω',
                path = '🖫',
                nvim_lua = 'Π',
            }

            item.menu = menu_icon[entry.source.name]
            return item
        end,
    },

    sources = {
        { name = 'nvim_lsp', keyword_length = 1 },
        { name = 'luasnip', keyword_length = 2 },
        { name = 'buffer', keyword_length = 2 },
        { name = 'path' },
    }
})


-- TODO Only for nixos? Set language server global
lsp.configure('lua_ls', {
    force_setup = true,
})
lsp.configure('pylsp', {
    force_setup = true,
})

-- BOTTOM
lsp.setup()

-- You need to setup `cmp` after lsp-zero
local cmp = require('cmp')
local cmp_action = require('lsp-zero').cmp_action()

cmp.setup({
  mapping = {
    -- `Enter` key to confirm completion
    ['<CR>'] = cmp.mapping.confirm({select = false}),

    -- Ctrl+Space to trigger completion menu
    ['<C-Space>'] = cmp.mapping.complete(),

    -- Navigate between snippet placeholder
    ['<C-f>'] = cmp_action.luasnip_jump_forward(),
    ['<C-b>'] = cmp_action.luasnip_jump_backward(),
  }
})

-- LSP Status
require "fidget".setup {}

vim.diagnostic.config({
    virtual_text = true,
    signs = true,
    update_in_insert = false,
    underline = true,
    severity_sort = false,
    float = {
        focusable = false,
      style = "minimal",
      border = "rounded",
      source = "always",
      header = "",
      prefix = "",

    },
})
