-- All the options
require"user.options"
require"user.keymaps"
-- Plugins
require"user.plugins"
-- LSP
require"user.lsp"
require"user.treesitter"
require"user.gitsigns"
require"user.rust"

-- Set Style
require"user.colorscheme"
require"user.bufferline"
require"user.lualine"

vim.lsp.set_log_level("debug")
