{
  inputs.nixpkgs.url = github:NixOS/nixpkgs;
  inputs.nixpkgs-unstable.url = github:NixOS/nixpkgs/nixpkgs-unstable;
  inputs.home-manager.url = github:nix-community/home-manager;
  
  outputs = { self, nixpkgs, nixpkgs-unstable, ... }@attrs: {
    nixosConfigurations.nixosVM = nixpkgs.lib.nixosSystem {
      system = "x86_64-linux";
      specialArgs = attrs;
      modules = [ ./configuration.nix ];
    };
  };
}
