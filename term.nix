{

  programs.kitty = {
    enable = true;
    theme = "Gruvbox Dark Hard";
    settings = {
      font_family = "JetBrainsMono";
      font_size = 12;
      force_ltr = "no";
      disable_ligatures = "never";
      enable_audio_bell = "no";
      confirm_os_window_close = -1;


    };
  };

}
